#
# Generic RPM build script Makefile
#
# License: GPLv2
#
# Run 'make prereq builddeps' before using.
#
# Adjustable Settings:
#
# * DIST
#     When empty (not set) then packages will be built for the current system.
#     Specifying DIST=<mock-configuration-name> will build packages with mock,
#     using the corresponding mock configuration file.
#     A '.cfg' extension will be removed and then its basename will be used, so
#     the value can be the full path of the mock configuration file, its
#     basename or its basename without the '.cfg' extension (its clean name).
#     Default: empty (not set)
# * VERBOSE
#     When set to non-zero then builds are verbose.
#     Default: 1
# * MAXDEPTH
#     This number of (sub)directory levels deep will be searched for spec files
#     to use.
#     Default: 2
# * FLATOUTPUT
#     When set to non-zero then the packages will not be put in a tree (with
#     the layout of a package repository) but in the directories configured by
#     the SRPMS_DIR and RPMS_DIR settings.
#     Default: 0
# * FORCE
#     When set to non-zero will forgo change detection and force a build of
#     every package.
#     Default: 0
# * BUILDALL
#     When set to non-zero will build all packages when none were built
#     because no changes were detected.
#     Default: 0
# * RPMS_DIR
#     The directory where the RPMs will be placed.
#     Default: <path-to-Makefile>/RPMS
# * SRPMS_DIR
#     The directory where the source RPMs will be placed (only takes effect for
#     flat output).
#     Default: <path-to-Makefile>/RPMS
#
#
# First the file 'Makefile.include' will be included when it exists.
# Custom settings and targets can be defined in this file.
#
# The Makefile assumes that the 'buildRPMs' script is in the same directory
# as the Makefile.
#


-include Makefile.include


DIST?=
VERBOSE?=1
MAXDEPTH?=2
FLATOUTPUT?=0
FORCE?=0
BUILDALL?=0
FEDORA=$(shell rpm -E '%{?fedora}')

export DIST
export VERBOSE
export MAXDEPTH
export FLATOUTPUT
export FORCE
export BUILDALL




GIT_COMMIT?=
GIT_PREVIOUS_SUCCESSFUL_COMMIT?=


UNIQUEEXT:=
DIST_CLEAN:=$(shell basename "$(DIST:.cfg=)")
MOCK_MODE:=
ifneq ($(DIST_CLEAN),)
  MOCK_MODE:=-M $(DIST_CLEAN)
endif


ifeq ($(VERBOSE),0)
  BUILDRPMSSCRIPTQUIET:=-q
  MKDIRCPRMMVVERBOSE:=
else
  BUILDRPMSSCRIPTQUIET:=
  MKDIRCPRMMVVERBOSE:=-v
endif


ifeq ($(FLATOUTPUT),0)
  FLATOUTPUTOPTION:=
else
  FLATOUTPUTOPTION:=-f
endif


ifeq ($(FORCE),0)
  FORCEOUTPUTOPTION:=
else
  FORCEOUTPUTOPTION:=-F
endif


ifeq ($(BUILDALL),0)
  ALLOUTPUTOPTION:=
else
  ALLOUTPUTOPTION:=-A
endif


MAKE_FILE:= $(realpath $(lastword $(MAKEFILE_LIST)))
MAKE_DIR := $(shell dirname "$(MAKE_FILE)")
CURR_DIR := $(shell pwd)
BUILDRPMS := $(MAKE_DIR)/buildRPMs

RPMS_DIR  ?= $(CURR_DIR)/RPMS
SRPMS_DIR ?= $(RPMS_DIR)

export RPMS_DIR
export SRPMS_DIR

SPECS     = $(shell find "$(CURR_DIR)" \
                -mindepth 1 \
                -maxdepth $(MAXDEPTH) \
                -type f \
                -name '*.spec' 2> /dev/null | sort )
SRPMS     = $(shell find "$(SRPMS_DIR)" \
                -maxdepth 1 \
                -type f \
                -name '*.src.rpm' 2> /dev/null | sort )




#
# Generic targets
#


.PHONY: all clean everything builddeps check srpms rpms


all: everything


clean:
	@rm $(MKDIRCPRMMVVERBOSE) -fr "$(RPMS_DIR)" "$(SRPMS_DIR)"


everything:
	@if [[ -n "$(SPECS)" ]]; then $(BUILDRPMS) \
          $(BUILDRPMSSCRIPTQUIET) \
          $(MOCK_MODE) \
          $(FLATOUTPUTOPTION) \
          $(FORCEOUTPUTOPTION) \
          $(ALLOUTPUTOPTION) \
          -m all \
          -s "$(SRPMS_DIR)" \
          -r "$(RPMS_DIR)" \
          -l "$(GIT_PREVIOUS_SUCCESSFUL_COMMIT)" \
          -c "$(GIT_COMMIT)" \
          $(SPECS); fi


builddeps:
	@if [[ -n "$(SPECS)" ]]; then $(BUILDRPMS) \
          $(BUILDRPMSSCRIPTQUIET) \
          $(MOCK_MODE) \
          $(FLATOUTPUTOPTION) \
          $(FORCEOUTPUTOPTION) \
          $(ALLOUTPUTOPTION) \
          -m builddeps \
          -s "$(SRPMS_DIR)" \
          -r "$(RPMS_DIR)" \
          -l "$(GIT_PREVIOUS_SUCCESSFUL_COMMIT)" \
          -c "$(GIT_COMMIT)" \
          $(SPECS); fi


check:
	@if [[ -n "$(SPECS)" ]]; then $(BUILDRPMS) \
          $(BUILDRPMSSCRIPTQUIET) \
          $(MOCK_MODE) \
          $(FLATOUTPUTOPTION) \
          $(FORCEOUTPUTOPTION) \
          $(ALLOUTPUTOPTION) \
          -m check \
          -s "$(SRPMS_DIR)" \
          -r "$(RPMS_DIR)" \
          -l "$(GIT_PREVIOUS_SUCCESSFUL_COMMIT)" \
          -c "$(GIT_COMMIT)" \
          $(SPECS); fi


srpms:
	@if [[ -n "$(SPECS)" ]]; then $(BUILDRPMS) \
          $(BUILDRPMSSCRIPTQUIET) \
          $(MOCK_MODE) \
          $(FLATOUTPUTOPTION) \
          $(FORCEOUTPUTOPTION) \
          $(ALLOUTPUTOPTION) \
          -m srpm \
          -s "$(SRPMS_DIR)" \
          -r "$(RPMS_DIR)" \
          -l "$(GIT_PREVIOUS_SUCCESSFUL_COMMIT)" \
          -c "$(GIT_COMMIT)" \
          $(SPECS); fi


rpms:
	@if [[ -n "$(SPECS)" ]]; then $(BUILDRPMS) \
          $(BUILDRPMSSCRIPTQUIET) \
          $(MOCK_MODE) \
          $(FLATOUTPUTOPTION) \
          $(FORCEOUTPUTOPTION) \
          $(ALLOUTPUTOPTION) \
          -m rpm \
          -s "$(SRPMS_DIR)" \
          -r "$(RPMS_DIR)" \
          -l "$(GIT_PREVIOUS_SUCCESSFUL_COMMIT)" \
          -c "$(GIT_COMMIT)" \
          $(SPECS); fi


#
# Distribution specific targets
#

.PHONY: prereq fedora fedora-rpmfusion

prereq:
	@sudo dnf install -y \
     bash coreutils dnf findutils gawk git grep gzip make mock python \
     redhat-lsb-core rpm rpm-build rpmdevtools rpmlint sed sudo tar

fedora:
	@$(MAKE) -f "$(MAKE_FILE)" DIST="fedora-$(FEDORA)-x86_64"

fedora-rpmfusion:
	@$(MAKE) -f "$(MAKE_FILE)" DIST="fedora-$(FEDORA)-x86_64-rpmfusion_free"

